"""
A pipeline for generating indexes and reference files. 
==============================================================================
"""
from snakemake.utils import makedirs
from os.path import abspath
import ftplib

ABSOLUTE_PATH = os.path.abspath("") + "/"


def check_for_file_in_ftp(link, wanted_file, server='ftp.ensembl.org'):
    """ For dealing with irregular hosting. 
    """
    ftp = ftplib.FTP(server)
    ftp.login()
    ftp.cwd(link)

    filelist = []
    ftp.retrlines('LIST', filelist.append)

    for f in filelist:
        if wanted_file in f:
            return True
    return False


GENOME_ASSEMBLY = "GRCh38"
ENSEMBL_VERSION = "94"
ORGANISM        = "Homo sapiens"
ORGANISM_lower_no_spaces = ORGANISM.replace(" ", "_").lower()
ORGANISM_upper  = ORGANISM_lower_no_spaces.capitalize()
TAX_ID          = "9606"

FTP_URL          = "ftp://ftp.ensembl.org"
VERSION_PATH     = "/pub/release-%s/" % ENSEMBL_VERSION
GENOME_PATH      = "fasta/%s/dna/" % ORGANISM_lower_no_spaces
PRIMARY_ASSEMBLY = "%s.%s.dna.primary_assembly.fa.gz" % (ORGANISM_upper, GENOME_ASSEMBLY)
TOPLEVEL         = "%s.%s.dna.toplevel.fa.gz" % (ORGANISM_upper, GENOME_ASSEMBLY)

GTF_PATH     = "gtf/%s/%s.%s.%s.gtf.gz" % (ORGANISM_lower_no_spaces, ORGANISM_upper, GENOME_ASSEMBLY, ENSEMBL_VERSION)
SNP_PATH     = "variation/vcf/%s/"      % (ORGANISM_lower_no_spaces)
SNP_FILE     = "%s.vcf.gz"              % (ORGANISM_lower_no_spaces)
ALT_SNP_FILE = "%s-chr*.vcf.gz"              % (ORGANISM_lower_no_spaces)

SPN_LOCATION = SNP_PATH + SNP_FILE
DATABASE = "%s_core_%s_38"          % (ORGANISM_lower_no_spaces, ENSEMBL_VERSION)

REFERENCE_PATH       = "references/"
ORGANISM_AND_VERSION = "%s/e%s/" % (ORGANISM_lower_no_spaces, ENSEMBL_VERSION)
REFERENCE_GENOME     = REFERENCE_PATH + ORGANISM_AND_VERSION + "genome.fa"
GENE_ANNOTATIONS     = REFERENCE_PATH + ORGANISM_AND_VERSION + "gene_annotations.gtf"
SNPs                 = REFERENCE_PATH + ORGANISM_AND_VERSION + "SNPs.vcf"
REPEATS              = REFERENCE_PATH + ORGANISM_AND_VERSION + "repeats.bed"


rule all:
    input: 
        REFERENCE_GENOME,
        GENE_ANNOTATIONS,
        REPEATS,
        SNPs

rule download_genome: 
    params: 
        url      = FTP_URL, 
        version  = VERSION_PATH,
        genome   = GENOME_PATH,
        primary  = PRIMARY_ASSEMBLY,
        toplevel = TOPLEVEL
    output: REFERENCE_GENOME
    run:
        if check_for_file_in_ftp(params.version + params.genome, params.primary):
            # In human and mouse toplevel contains haplotypes, which are bad for aligners. 
            # Thus we need to get the "primary assemblies" which are free of haplotypes
            shell(""" wget -O- {params.url}{params.version}{params.genome}{params.primary}  | gunzip -c > {output} """)
        else:
            shell(""" wget -O- {params.url}{params.version}{params.genome}{params.toplevel} | gunzip -c > {output} """)


rule download_gene_annotations: 
    params:
        url      = FTP_URL, 
        version  = VERSION_PATH,
        gtf_path = GTF_PATH
    output: GENE_ANNOTATIONS
    shell: """ wget {params.url}{params.version}{params.gtf_path} --output-file {output} """ 


rule download_SNPs: 
    params:
        url          = FTP_URL, 
        version      = VERSION_PATH,
        snp_path     = SNP_PATH,
        snp_file     = SNP_FILE,
        snp_wildcard = ALT_SNP_FILE
    output: SNPs
    run:
        if check_for_file_in_ftp(params.version + params.snp_path, params.snp_file):
            shell(""" wget -O- {params.url}{params.version}{params.genome}{params.primary} | gunzip -c > {output} """)
        else:
            shell("""
                wget -O- -r --no-parent                       \
                -A '{params.snp_wildcard}'                    \
                {params.url}{params.version}{params.snp_path} \
                | gunzip -c | grep -v "#" > {output}
             """)


rule get_repeatative_elements:
    params: database = DATABASE
    output: REPEATS
    shell: """
        mysql -u anonymous -h ensembldb.ensembl.org --batch {params.database}                              \
        -e ' \
        SELECT seq_region.name, repeat_feature.seq_region_start, repeat_feature.seq_region_end,        \
        repeat_consensus.repeat_name, "0", repeat_feature.seq_region_strand,                               \
        repeat_consensus.repeat_class, seq_region.seq_region_id, seq_region.coord_system_id                \
        FROM repeat_feature                                                                                \
        JOIN repeat_consensus ON repeat_feature.repeat_consensus_id = repeat_consensus.repeat_consensus_id \
        JOIN seq_region       ON repeat_feature.seq_region_id       = seq_region.seq_region_id             \
        ' \
        > {output}
    """

