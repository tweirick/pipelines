import os
from snakemake.utils import makedirs

ABSP = os.path.abspath("") + "/"

FASTQ_DIR = "FASTQS/"
makedirs(ABSP + FASTQ_DIR)
FASTQ_FILE = FASTQ_DIR + "{sample}.fastq"

DATASETS = []
for line in open(config["samples"]):
    DATASETS.append(line.strip())

rule all:
    input:
        expand(FASTQ_FILE, sample=DATASETS)

rule fastqdump:
    output: FASTQ_FILE
    shell: """fastq-dump --stdout {wildcards.sample} > {output} """   

