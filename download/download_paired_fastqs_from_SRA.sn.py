import os
from snakemake.utils import makedirs

ABSP = os.path.abspath("") + "/"

FASTQ_DIR = "FASTQS/"
makedirs(ABSP + FASTQ_DIR)
FASTQ_1 = FASTQ_DIR + "{sample}_1.fastq"
FASTQ_2 = FASTQ_DIR + "{sample}_2.fastq"


DATASETS = []
for line in open(config["samples"]):
    DATASETS.append(line.strip())


rule all:
    input:
        expand(FASTQ_1, sample=DATASETS),


rule fastqdump:
    output:
        fastq_1 = FASTQ_1,
        fastq_2 = FASTQ_2
    params: fastq_dir = FASTQ_DIR
    shell: """fastq-dump --split-3 --O {params.fastq_dir} {wildcards.sample} ; touch -c {output.fastq_1}; touch -c {output.fastq_2};"""   



