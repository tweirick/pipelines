import os
from snakemake.utils import makedirs
import json
import time
import os
from ftplib import FTP

ABSP = os.path.abspath("") + "/"

DATASETS = []
for line in open(config["samples"]):
    DATASETS.append(line.strip())

FASTQ_DIR = "FASTQS/"
makedirs(ABSP + FASTQ_DIR)
FASTQDUMP_FILE_1 = FASTQ_DIR + "{sample}_1.fastq"
FASTQDUMP_FILE_2 = FASTQ_DIR + "{sample}_2.fastq"

BT2_DIR = "BT2/" 
makedirs(ABSP + BT2_DIR)
BT2_OUT = BT2_DIR + "{sample}.sam"
BT2_LOG = BT2_DIR + "{sample}.log"
SORTED_BAM = BT2_DIR + "{sample}.sorted.bam"
UNMAPPED_BAM = BT2_DIR + "{sample}.unmapped.bam"

CIRCBASE_DIR = "CIRCBASE/"
makedirs(ABSP + CIRCBASE_DIR)
ANCHORS   = CIRCBASE_DIR + "{sample}_anchors.fastq"
REMAPPED  = CIRCBASE_DIR + "{sample}.remapped.sam"
CIRCBED   = CIRCBASE_DIR + "{sample}.bed"
SITES     = CIRCBASE_DIR + "{sample}_sites.bed"
SITES_LOG = CIRCBASE_DIR + "{sample}_circ.log"
FILTERED_CIRCRNAS = CIRCBASE_DIR + "{sample}_filtered.bed"

START_REGION_FASTA = "start.fna"
STOP_REGION_FASTA  = "stop.fna"
START_REGION_BED = "start.bed"
STOP_REGION_BED = "stop.bed"

MERGED_CIRCRNAS_WITH_UGAS = "merged_circRNAs.withugas-bed"


rule all:
    input:
        expand(FILTERED_CIRCRNAS, sample=DATASETS),
        MERGED_CIRCRNAS_WITH_UGAS

'''
rule fastqdump:
    output:
        fastq_1 = FASTQDUMP_FILE_1,
        fastq_2 = FASTQDUMP_FILE_2
    params: fastq_dir = FASTQ_DIR
    priority: 1
    shell: """fastq-dump --split-3 --O {params.fastq_dir} {wildcards.sample}; touch -c {output.fastq_1}; touch -c {output.fastq_2};""" 


rule bowtie2_paired: 
    input: 
        fastq_1 = FASTQDUMP_FILE_1,
        fastq_2 = FASTQDUMP_FILE_2
    params: 
        reference_genome = config["reference_genome"]
    output:
        sam = BT2_OUT,
        log = BT2_LOG
    priority: 2
    shell: """ bowtie2 -p 30 --very-sensitive --mm --score-min=C,-15,0 -x {params.reference_genome} \
    -1 {input.fastq_1} -2 {input.fastq_2} 2> {output.log} > {output.sam}
    """

rule convert_to_bam: 
    input:  BT2_OUT
    output: SORTED_BAM 
    priority: 3
    shell: """samtools view -hbuS {input} | samtools sort -o {output}"""


rule find_unmapped: 
    input: SORTED_BAM
    output: UNMAPPED_BAM 
    priority: 4
    shell: """ samtools view -b -hf 4 {input} > {output} """
'''

rule create_anchors: 
    input: UNMAPPED_BAM
    output: ANCHORS
    priority: 5
    shell: """
    module load python-2.7.12 
    unmapped2anchors.py {input} > {output} """


rule remap:
    input: ANCHORS
    output: REMAPPED
    params: reference_genome = config["reference_genome"]
    priority: 6
    shell: """bowtie2 -p 30 --reorder --mm --score-min=C,-15,0 -q -x {params.reference_genome} -U {input} > {output}  """


rule find_circRNA:
    input: REMAPPED
    params: reference_genome = config["reference_genome"]
    output:         
        log = SITES_LOG,
        bed = CIRCBED,
        sites_bed = SITES
    priority: 7
    shell: """
    module load python-2.7.12 
    cat {input} | find_circ.py --genome={params.reference_genome} --prefix=p{wildcards.sample} --name={wildcards.sample} --stats={output.log} --reads={output.bed} > {output.sites_bed}
    """

# awk '!match($1,/MT/)&&match($4,/circ/) && $7>=2 && $8>=35 && $9>=35 && $3-$2<=100000{{print $0,$3-$2}}' {input} > {output} 
# grep -vP "MT\\t"
rule filter_RNAs: 
    input: SITES
    output: FILTERED_CIRCRNAS 
    priority: 8
    shell: """ 
    module load python-2.7.12 
    grep CIRCULAR {input} | grep -vP "MT\\t" | awk '$5>=2' | grep UNAMBIGUOUS_BP | grep ANCHOR_UNIQUE | maxlength.py 100000 > {output} """

MERGED_CIRCRNAS = "merged_circRNAs.bed"
rule merge_circRNA_bed:
    input: expand(FILTERED_CIRCRNAS, sample=DATASETS),
    output:
        stats = "circRNA_merge_stats.txt",
        merged_circRNAs = MERGED_CIRCRNAS
    shell: """ 
    module load python-2.7.12
    merge_bed.py -s {output.stats} {input} > {output.merged_circRNAs} 
    """

#module load python-2.7.12
MERGED_CIRCRNAS_WITH_UGAS = "merged_circRNAs.withugas-bed"
rule get_ugas_region:
    input: MERGED_CIRCRNAS
    output: MERGED_CIRCRNAS_WITH_UGAS
    shell: """
    module load python-3.5.2
    bedtouga.py GRCm38 CR {input} | vectools slice --keep-cols 0,1,2,-1,4,5:,3 STDIN > {output}
    """

rule get_start_region:
    input: MERGED_CIRCRNAS_WITH_UGAS
    output: START_REGION_BED
    shell: """ awk '{print $1"\t"$2-100"\t"$2+100"\t"$4"\t"$5"\t"$6}' {input} > {output} """

rule get_stop_region:
    input: MERGED_CIRCRNAS_WITH_UGAS
    output: STOP_REGION_BED
    shell: """ awk '{print $1"\t"$3-100"\t"$3+100"\t"$4"\t"$5"\t"$6}' {input} > {output} """

rule extract_start_region_fasta:
    input: START_REGION_BED
    output: START_REGION_FASTA
    params: reference_genome = config["reference_genome"]
    shell: """bedtools getfasta -fi {parmas.reference_genome} -bed {input} -fo {output}"""

rule extract_stop_region_fasta:
    input: STOP_REGION_BED
    output: STOP_REGION_FASTA
    shell: """bedtools getfasta -fi {parmas.reference_genome} -bed {input} -fo {output}"""

rule add_start_region_to_stop_region:
    input:
       start_fasta_vec = START_REGION_FASTA,
       stop_fasta_vec = STOP_REGION_FASTA
    output: "circRNA_junctions_pm100bp.fna"
    shell: """ vectools add --only-apply-on 1 {input.stop} {input.start} """


