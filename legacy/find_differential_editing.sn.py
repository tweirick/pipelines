import os
from snakemake.utils import makedirs
ABSP = os.path.abspath("") + "/"

DATA = {}
DATASETS = []
for line in open(config["samples"]):
    sample, group = line.strip().split()
    DATASETS.append(sample)
    try:
        DATA[group].append(sample)
    except KeyError:
        DATA[group] = [sample]

MIN_EDITING      = int(config["min_editing"])
MIN_COVERAGE     = int(config["min_coverage"])
MERGED_SITES     = config["merged_sites"]
ISLANDS          = config["islands"]
REFERENCE_GENOME = config["reference_genome"]
ALU_REGIONS      = config["alu_regions"]
QSCORES_LIST     = config["qscores"]

ONE_TO_ONE_BENCHMARK_COUNT  = 1
MANY_TO_ONE_BENCHMARK_COUNT = 1
# ============================================================================
#                              Initial Files
# ============================================================================
BAM_DIR = "PURIFICATION//"
BAM_FILE = BAM_DIR + "{sample}.noDup.realigned.recalibrated.bam"
BAI_FILE = BAM_DIR + "{sample}.noDup.realigned.recalibrated.bai"
# ============================================================================
#                              Files to Create
# ============================================================================
EPK_DIR  = "EPK/"
makedirs(ABSP + EPK_DIR)
SAMPLE_EPK  = EPK_DIR + "{sample}.q{qscore}.sample_EPK.tsv"
ISLAND_EPK  = EPK_DIR + "{sample}.q{qscore}.island_EPK.tsv"
SITE_EPK    = EPK_DIR + "{sample}.q{qscore}.edsite_EPK.tsv"

AEI_DIR  = "AEI/"
makedirs(ABSP + EPK_DIR)
SAMPLE_ALU_AEI  = AEI_DIR + "{sample}.q{qscore}.sample_aluAEI.tsv"

PLOTS = "PLOTS/"
makedirs(ABSP + PLOTS)
GROUPED_EPK_DATA = PLOTS + "{group}_{qscore}_sample_EPK__Ref_vs_Alt_bases.tsv"
SCATTER_PLOT     = PLOTS + "{group}_{qscore}_sample_EPK__Ref_vs_Alt_bases.svg"
R2_VAL           = PLOTS + "{group}_{qscore}_sample_EPK__Ref_vs_Alt_bases.r2_val.tsv"

GROUPED_AEI_DATA = PLOTS + "{group}_{qscore}_sample_aAEI__Ref_vs_Alt_bases.tsv"
SCATTER_PLOT_AEI = PLOTS + "{group}_{qscore}_sample_aAEI__Ref_vs_Alt_bases.svg"
R2_VAL_AEI       = PLOTS + "{group}_{qscore}_sample_aAEI__Ref_vs_Alt_bases.r2_val.tsv"

DIFFED_DIR = "DIFFED/"
makedirs(ABSP + DIFFED_DIR)
SITEWISE   = DIFFED_DIR + "diff_ed_sites.tsv"
REGIONWISE = DIFFED_DIR + "diff_ed_regions.tsv"
# ============================================================================
#                                   ALL
# ============================================================================

rule all:
    input:
        #expand(SAMPLE_EPK,     sample=DATASETS, qscore=QSCORES_LIST),
        expand(GROUPED_EPK_DATA, group=list(DATA), qscore=QSCORES_LIST),
        expand(R2_VAL, group=list(DATA), qscore=QSCORES_LIST),
        expand(GROUPED_AEI_DATA, group=list(DATA), qscore=QSCORES_LIST),
        expand(R2_VAL_AEI, group=list(DATA), qscore=QSCORES_LIST),
        #expand(ISLAND_EPK,     sample=DATASETS, qscore=QSCORES_LIST),
        #expand(SITE_EPK,       sample=DATASETS, qscore=QSCORES_LIST),
        #expand(SAMPLE_EDI_AEI, sample=DATASETS, qscore=QSCORES_LIST),
        #expand(SAMPLE_ALU_AEI, sample=DATASETS, qscore=QSCORES_LIST),
        #SITEWISE,
        #REGIONWISE

# ============================================================================
#                                  Rules
# ============================================================================

rule find_sample_epk:
    input: BAM_FILE
    params: merged_editing_sites = MERGED_SITES
    output: SAMPLE_EPK
    run:
        shell("""
        dretools sample-epk                      \
            --name {wildcards.sample}            \
            --min-qscore {wildcards.qscore}      \
            --vcf {params.merged_editing_sites}  \
            --min-qscore {wildcards.qscore}      \
            --alignment {input} > {output} """)



def bygroupandqscore(wildcards):
    return expand(SAMPLE_EPK, sample=DATA[wildcards.group], qscore=wildcards.qscore)

rule combine_groups:
    input: bygroupandqscore
    output: GROUPED_EPK_DATA
    shell: """ vectools concat --axis 0 -c {input} > {output} """


rule make_epk_scatter_plots:
    input: GROUPED_EPK_DATA
    output:
        svg_plot = SCATTER_PLOT,
        r2_value = R2_VAL
    shell: """
    module load R-3.4.0-microsoft;
    make_scatter_plot.R {input} {output.svg_plot} {output.r2_value} """



rule find_sample_alu_aei:
    input: BAM_FILE
    params:
        genome         = REFERENCE_GENOME,
        merged_edsites = MERGED_SITES,
        regions        = ALU_REGIONS
    output: SAMPLE_ALU_AEI
    run:
        shell("""
        dretools sample-aei \
           --name      {wildcards.sample}      \
           --genome    {params.genome}         \
           --vcf       {params.merged_edsites} \
           --regions   {params.regions}        \
           --min-qscore {wildcards.qscore}     \
           --alignment {input} > {output}
       """)


def bygroupandqscore_aei(wildcards):
    return expand(SAMPLE_ALU_AEI, sample=DATA[wildcards.group], qscore=wildcards.qscore)

rule combine_groups_aei:
    input: bygroupandqscore_aei
    output: GROUPED_AEI_DATA
    shell: """ vectools concat --axis 0 -c {input} > {output} """


rule make_epk_scatter_plots_aei:
    input: GROUPED_AEI_DATA
    output:
        svg_plot = SCATTER_PLOT_AEI,
        r2_value = R2_VAL_AEI
    shell: """
    module load R-3.4.0-microsoft ;
    make_scatter_plot.R {input} {output.svg_plot} {output.r2_value} """



'''
rule find_site_epk:
    input: BAM_FILE
    params: merged_editing_sites = MERGED_SITES
    output: SITE_EPK
    run:
        shell("""
        dretools edsite-epk                      \
            --vcf {params.merged_editing_sites}  \
            --min-qscore {wildcards.qscore}      \
            --alignment  {input} > {output} """, bench_record=bench_record)



rule find_island_epk:
    input: BAM_FILE
    params:
        merged_editing_sites = MERGED_SITES,
        regions = ISLANDS
    output: ISLAND_EPK
    benchmark: repeat(ISLAND_EPK + ".benchmarks.txt", ONE_TO_ONE_BENCHMARK_COUNT)
    run:
        shell("""
        dretools region-epk \
        --vcf     {params.merged_editing_sites}  \
        --regions {params.regions}               \
        --min-qscore {wildcards.qscore}          \
        --alignment {input} > {output} """, bench_record=bench_record)


rule regionwise_differential_editing:
    input:
        samples = expand(SAMPLE_EPK, sample=DATASETS),
        regions = expand(ISLAND_EPK, sample=DATASETS)
    params:
        sample_groups = " ".join([",".join(expand(SAMPLE_EPK, sample=DATA[g])) for g in sorted(DATA)]),
        region_groups = " ".join([",".join(expand(ISLAND_EPK, sample=DATA[g])) for g in sorted(DATA)]),
        regions = ISLANDS,
        groups = ",".join(sorted(DATA))
    output: REGIONWISE
    benchmark: repeat(REGIONWISE + ".benchmarks.txt", MANY_TO_ONE_BENCHMARK_COUNT)
    run:
        shell("""
        touch --no-create {input.samples};
        touch --no-create {input.regions};
        dretools region-diff               \
       --regions {params.regions}          \
       --min-depth 5                       \
       --names {params.groups}             \
       --sample-epk {params.sample_groups} \
       --region-epk {params.region_groups} \
       > {output}
         """, bench_record=bench_record)


rule sitewise_differential_editing:
    input:
        samples = expand(SAMPLE_EPK, sample=DATASETS),
        sites = expand(SITE_EPK, sample=DATASETS)
    params:
        sample_groups = " ".join([",".join(expand(SAMPLE_EPK, sample=DATA[g])) for g in sorted(DATA)]),
        site_groups = " ".join([",".join(expand(SITE_EPK, sample=DATA[g])) for g in sorted(DATA)]),
        groups = ",".join(sorted(DATA)),
        sites = MERGED_SITES
    output: SITEWISE
    benchmark: repeat(SITEWISE + ".benchmarks.txt", MANY_TO_ONE_BENCHMARK_COUNT)
    run:
        shell("""
        touch --no-create {input.samples};
        touch --no-create {input.sites};
        dretools edsite-diff               \
       --min-depth 5                       \
       --sites {params.sites}              \
       --names      {params.groups}        \
       --sample-epk {params.sample_groups} \
       --site-epk   {params.site_groups}   \
       > {output}
         """, bench_record=bench_record)
'''
