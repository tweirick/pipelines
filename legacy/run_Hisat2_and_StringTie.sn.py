import os
from snakemake.utils import makedirs
configfile: "config.json"

ABSP = os.path.abspath("") + "/"
TAXID = "10090"


FASTQ_DIR = "FASTQS/"
makedirs(ABSP + FASTQ_DIR)
FASTQ_1 = FASTQ_DIR + "{sample}_1.fastq"
FASTQ_2 = FASTQ_DIR + "{sample}_2.fastq"
#FASTQ_1 = FASTQ_DIR + "{sample}_1.fastq"
#FASTQ_2 = FASTQ_DIR + "{sample}_2.fastq"
TRIMMED_PAIRED_FASTQ_1   = FASTQ_DIR + "{sample}_paired_1.fastq"
TRIMMED_PAIRED_FASTQ_2   = FASTQ_DIR + "{sample}_paired_2.fastq"
TRIMMED_UNPAIRED_FASTQ_1 = FASTQ_DIR + "{sample}_unpaired_1.fastq"
TRIMMED_UNPAIRED_FASTQ_2 = FASTQ_DIR + "{sample}_unpaired_2.fastq"

HISAT2_DIR = "HISAT2/"
makedirs(ABSP + HISAT2_DIR)
HISAT2_SAM       = HISAT2_DIR + "{sample}.sam"
HISAT2_STATS     = HISAT2_DIR + "{sample}.stats"
HISAT2_BAM       = HISAT2_DIR + "{sample}.bam"
HISAT2_BAI       = HISAT2_DIR + "{sample}.bam.bai"
HISAT2_READ_DIST = HISAT2_DIR + "{sample}.read_dist.txt"


STRINGTIE_DIR = "STRINGTIE/"
makedirs(ABSP + STRINGTIE_DIR)
STRINGTIE_GENES = STRINGTIE_DIR + "{sample}.genes.tsv" 
STRINGTIE_GTF   = STRINGTIE_DIR + "{sample}.gtf"

MERGED_GTF_FOLDER = "MERGED_GTF/"
makedirs(ABSP + MERGED_GTF_FOLDER)
MERGED_CONDITONS  = MERGED_GTF_FOLDER + "%s.merge_list.txt" % TAXID
MERGED_GTF_FILE   = MERGED_GTF_FOLDER + "merged.%s.gtf" % TAXID

#CUFFQUANT_FOLDER = "CUFFQUANT/"
#CUFFQUANT_CBX = CUFFQUANT_FOLDER + "{sample}.cbx"
#CUFFQUANT_FOLDER = "CUFFQUANT/{tax_id}/{sample}/"

CUFFQUANT_FOLDER = "CUFFQUANT/%s/{sample}/" % TAXID
makedirs(ABSP + CUFFQUANT_FOLDER)
CUFFQUANT_CXB    = CUFFQUANT_FOLDER + "abundances.cxb"

CUFFNORM_FOLDER = "CUFFNORM/"
CUFFNORM_CONDITONS_FILE = CUFFNORM_FOLDER + "%s.cuffnorm_conditions.txt" % TAXID
CUFFNORM_OUT_DIR = CUFFNORM_FOLDER + "%s/" % TAXID
CUFFNORM_GENES_FPKM_TABLE      =  CUFFNORM_OUT_DIR + "genes.fpkm_table"
CUFFNORM_TRANSCRIPT_FPKM_TABLE =  CUFFNORM_OUT_DIR + "isoforms.fpkm_table"



DATASETS = []
for line in open(config["samples"]):
    DATASETS.append(line.strip())


rule all:
    input:
        expand(MERGED_GTF_FILE, sample=DATASETS),


rule trim_fastqs: 
    input:
        fastq_1 = FASTQ_1,
        fastq_2 = FASTQ_2 
    output: 
        trimmed_fastq_paired_1   = TRIMMED_PAIRED_FASTQ_1,
        trimmed_fastq_paired_2   = TRIMMED_PAIRED_FASTQ_2,
        trimmed_fastq_unpaired_1 = TRIMMED_UNPAIRED_FASTQ_1,
        trimmed_fastq_unpaired_2 = TRIMMED_UNPAIRED_FASTQ_2,
    shell: """
    module load java-1.8.0_40
    java -jar /scratch/global/tmweir01/bin/trimmomatic-0.36.jar PE -phred33 {input.fastq_1} {input.fastq_2}      \
                                               {output.trimmed_fastq_paired_1} {output.trimmed_fastq_unpaired_1} \
                                               {output.trimmed_fastq_paired_2} {output.trimmed_fastq_unpaired_2} \
                                               ILLUMINACLIP:all_fastq_primers.fa:2:30:10                         \
                                               LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
    """


rule HISAT2:
    input:
        fastq_1  = TRIMMED_PAIRED_FASTQ_1,
        fastq_2  = TRIMMED_PAIRED_FASTQ_2
    params:
        index = config["hisat2_index"]
    output:
        sam = HISAT2_SAM,
        # dbl_strn = HISAT2_DBL_UNALIGN,
        # sng_str = HISAT2_SNG_UNALIGN,
        stats = HISAT2_STATS
    shell: """ 
    hisat2 --dta-cufflinks --mm -p 8 {params.index} \
        -1 {input.fastq_1} -2 {input.fastq_2}       \
        --un-conc-gz {output.sam}.dbl_strn.gz       \
        --un-gz      {output.sam}.sng_str.gz        \
        -S {output.sam} > {output.stats}  2>&1   
    """


rule Sam_to_Bam:
    input: HISAT2_SAM
    output: HISAT2_BAM
    shadow: "shallow"
    shell:  """samtools sort -@ 8 -o {output} {input} """


rule make_bai:
    input: HISAT2_BAM
    output: HISAT2_BAI
    shell: """samtools index {input} {output} """


rule StringTie:
    input: HISAT2_BAM,
    params: ref_gtf = config["gtf"]
    output:
        genes = STRINGTIE_GENES,
        gtf   = STRINGTIE_GTF
    shell: """
    module load python-2.7.12
    stringtie -p 8 -G {params.ref_gtf} {input} -A {output.genes} -o {output.gtf}"""


rule generate_merge_samples_file:
    input:
        expand(STRINGTIE_GTF, sample=DATASETS)
    output:
        merge_list = MERGED_CONDITONS
    run:
        with open(output.merge_list, 'w') as out:
            print(*input, sep="\n", file=out)


rule stringtie_merge_gtfs:
    input: MERGED_CONDITONS
    params: ref_gtf = config["gtf"]
    output: MERGED_GTF_FILE
    shell: """
    module load python-2.7.12
    stringtie --merge -p 8 -G {params.ref_gtf} {input} -o {output}""" 


rule cuffquant:
    input:
        merged_gtf = MERGED_GTF_FILE,
        bam = HISAT2_BAM
    output:
        out_dir = CUFFQUANT_FOLDER,
        cxb = CUFFQUANT_CXB
    run:
        shell("mkdir -p {output.out_dir} ;")
        shell("""cuffquant -o {output.out_dir} {input.merged_gtf} {input.bam} """ )


rule generate_cuffnorm_samples_file:
    input:
        expand(CUFFQUANT_CXB, sample=DATASETS)
    output:
        cuffnorm_list = CUFFNORM_CONDITONS_FILE
    run:
        shell("mkdir -p %s ;" %  CUFFNORM_FOLDER)
        with open(output.cuffnorm_list, 'w') as out:
            print(*input, sep=" ", file=out)


rule cuffnorm:
    input:
        merged_gtf = MERGED_GTF_FILE,
        conditions = CUFFNORM_CONDITONS_FILE
    output:
        out_dir = CUFFNORM_OUT_DIR,
    run:
        shell("mkdir -p {output.out_dir} ;")

        conditions_text = open(input.conditions).read()

        shell("""cuffnorm --output-dir {output.out_dir} {input.merged_gtf} %s """ % conditions_text)



"""
cuffdiff /media/tyler/DATA/Storage/references/10090/e86/10090.e86.gtf \
    -p 8 \
    --output-dir Cuffdiff_RIP           \
    SU-01-Input-1.bam,SU-02-Input-2.bam \
    SU-03-IgG-1.bam,SU-04-IgG-2.bam     \
    SU-05-NSuN-1.bam,SU-06-NSuN-2.bam   \
    SU-07-Phi52-1.bam,SU-08-Phi52-2.bam \
    SU-09-TDP43-1.bam,SU-10-TDP43-2.bam
"""
