"""
{
    "samples":         "samples.txt"

    "min_editing:  3,
    "min_coverage: 5,
    "min_samples": 3,

    "min_length":  20,
    "min_points":  5,
    "epsilon":     100

    "INPUT_DIRS": ["NORMAL/RESULTS/", "SCSEQ/RESULTS/"],

}
"""
import os

# ============================================================================
#                              Helper functions
# ============================================================================

def check_file(file_path, file_not_found_message='File "%s" not found.'):
    assert os.path.isfile(file_path), file_not_found_message % file_path
    return file_path

def get_sample_names(sample_file):
    samples = []
    for line in open(sample_file):
        # Use the split("\t") command here so we can keep sample
        # ids paired with annotation information.
        samples.append(line.strip().split("\t")[0])
    return samples

MIN_EDITING  = int(config["min_editing"])
MIN_COVERAGE = int(config["min_coverage"])
MIN_SAMPLES  = int(config["min_samples"])
MIN_LENGTH   = int(config["min_length"])
MIN_POINTS   = int(config["min_points"])
MIN_EPSILON  = int(config["epsilon"])


#MERGED_SITES = check_file(config["merged_sites"])
#ISLANDS      = check_file(config["islands"])
IN_DIRS = config["INPUT_DIRS"]
SAMPLES = get_sample_names(config["samples"])

# =================================================================================================
#                              Initial Files
# =================================================================================================
#IN_DIR   = "{input_dirs}"
#EDITING_SITES = IN_DIR + "{sample}.all_editing_sites.vcf"
#EDITING_SITES = 
# =================================================================================================
#                              Files to Create
# =================================================================================================

OVERALL_DIR = "OVERALL/"

MERGE_STR = "".join([
    "merged_sites" +
    "_me%s" % MIN_EDITING,
    "_mc%s" % MIN_COVERAGE,
    "_ms%s" % MIN_SAMPLES,
    ".vcf"
])
MERGED_SITES = OVERALL_DIR + MERGE_STR

ISLAND_STR = "".join(["islands"+
    "_me%s" % MIN_EDITING,
    "_mc%s" % MIN_COVERAGE,
    "_ml%s" % MIN_LENGTH,
    "_mp%s" % MIN_POINTS,
    "_ep%s" % MIN_EPSILON, ".bed"
])
EDITING_ISLANDS = OVERALL_DIR + ISLAND_STR


# =================================================================================================
#                                   ALL
# =================================================================================================

rule all:
    input:
        expand(MERGED_SITES),
        expand(EDITING_ISLANDS)

# =================================================================================================
#                                  Rules
# =================================================================================================

rule make_editing_site_consensus_set:
    input: SAMPLES
    params:
        min_editing = MIN_EDITING,
        min_coverage = MIN_COVERAGE,
        min_samples = MIN_SAMPLES
    output: MERGED_SITES
    shell: """
        dretools edsite-merge                     \
            --min-editing  {params.min_editing}   \
            --min-coverage {params.min_coverage}  \
            --min-samples  {params.min_samples}   \
            --vcf {input} > {output}
    """

rule find_editing_islands:
    input: MERGED_SITES
    params:
        min_editing = MIN_EDITING,
        min_coverage = MIN_COVERAGE,
        min_length = MIN_LENGTH,
        min_points = MIN_POINTS,
        epsilon = MIN_EPSILON
    output: EDITING_ISLANDS
    shell: """
    dretools find-islands        \
        --min-editing  {params.min_editing} \
        --min-coverage {params.min_coverage} \
        --min-length   {params.min_length} \
        --min-points   {params.min_points} \
        --epsilon      {params.epsilon} \
        --vcf {input} > {output}
    """
