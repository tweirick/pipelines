"""
RNAEditorSM - RNAEditor SnakeMake Edition
==============================================================================

localrules: all, foo

Table of contents: 
- Introduction
- Installation 
- Running RNAEditorSM

== Introduction ==

This is an implmentation of the RNAEditor pipeline 
(https://doi.org/10.1093/bib/bbw087). In the original manuscript we published 
a GUI for finding RNA Editing sites. However, we have seen an increasing need
for an implementation able to run on HPC envioroments. RNAEditorSM is our 
solution to this. Implementation in Snakemake allows for automatic paralization 
across HPC clusters. 

== Installation ==

Required Software:

    Install following tools to /usr/local/bin/:

        BWA
        Picard Tools Move all .jar files to /usr/local/bin/picard-tools/ (use version <= 1.119)
        GATK Use GATK version 3.5 with java 1.7, the newer versions are currently causing problems.
        Blat
        Bedtools
        vectools (python3 -m pip install vectools)

    Required python packages:
        python3 -m pip install pysam       
        

== Running RNAEditorSM == 
To run RNAEditorSM two files are needed and depending on the HPC enviroment
it is likely that a third file is needed. Finally note that RNAEditorSM is 
only designed to work on paired end libararies. 

The two basic files are a config file are: 
- A config file. This file describes the locations of the programs and 
  files needed for the pipeline. 
- A sample file. This file describes the samples to run through the pipeline.
- (Optinal) a cluter config file. This file describes the HPC resources. We 
  give an example of this file. However, due to the variation in HPC
  enviroments we highly recomend consulting with your local HPC administrator 
  and the Snakemake documentation. 

Example config file (config.json). This file should be a json file with all the entries customized to your local 
system. 
{
    "samples":  "samples.txt",
    
    "stand_call": "1",
    "stand_emit": "1",
    "threads":    "1",
    "max_diff":   "0.04",
    "seed_diff":  "2",
    "distance":   "4",

    "trimmomatic_path": "bin/trimmomatic-0.36.jar",
    "trimmomatic_args": "ILLUMINACLIP:all_fastq_primers.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36",

    "SNPs":             "references/9606/e83/dbSNP.vcf",
    "editing_types":    "references/9606/e83/editing_types_from_base_to_base.txt",
    "reference_genome": "references/9606/e83/Homo_sapiens.GRCh38.dna.primary_assembly.fa",
    "repeats":          "references/9606/e83/repeats.bed",
    "gene_names":       "references/9606/e83/GRCh38.83.gtf.bed",
    "gtf":              "references/9606/e83/Homo_sapiens.GRCh38.83.gtf",
    "chromosomes":      "references/9606/e83/Homo_sapiens.GRCh38.83.chromosomes"
    "gatk_path":        "bin/GATK_3.7-0-gcfedb67/GenomeAnalysisTK.jar",
}

Example sample file (samples.txt)
samples.txt - Should contain one file ID per line. 
Ex: 
SRR5575725
SRR787276
TODO: Remember to finx is not None in vectools
 
"""

from snakemake.utils import makedirs
from os.path import abspath
localrules: prefetch, combine_filtered_editing_sites

ABSOLUTE_PATH = os.path.abspath("") + "/"
REPEATS          = config["repeats"]
DISTANCE         = config["distance"]
SNPS             = config["SNPs"]
STAND_CALL       = config["stand_call"]
STAND_EMIT       = config["stand_emit"]
THREADS          = config["threads"]
GATK_PATH        = config["gatk_path"]
PICARD_PATH      = config["picard_path"]

TMP_PATH = ABSOLUTE_PATH + "EDITING_TMP"
makedirs(TMP_PATH)

try:
    THREADS = int(THREADS)
except ValueError:
    exit("Error: Threads must be an integer.")

MAX_DIFF         = config["max_diff"]
SEED_DIFF        = config["seed_diff"]

REFERENCE_GENOME = config["reference_genome"]
SPLICE_REGIONS = config["splice_regions"]
REPEAT_REGIONS = config["repeat_regions"]
# ============================================================================
#                             Rule IO
# ============================================================================

SRA = "sra/{sample}.sra"

# @TODO: Allow setting this in the config file. If none found default to fastq
# ??? Or just have a rule to rename files? or make aliases?
# Fastq suffixes may be different depending on vendors.
FASTQ_SUFFIX = "fastq"
FASTQ_DIR = "FASTQS/"
SINGLE_FASTQ                    = FASTQ_DIR + "{sample}.%s" % FASTQ_SUFFIX
SINGLE_TRIMMED_FASTQ            = FASTQ_DIR + "{sample}_s.trimmed.%s" % FASTQ_SUFFIX

PAIRED_FASTQ_1                  = FASTQ_DIR + "{sample}_1.%s" % FASTQ_SUFFIX
PAIRED_FASTQ_2                  = FASTQ_DIR + "{sample}_2.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_FASTQ_1          = FASTQ_DIR + "{sample}_1.trimmed.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_FASTQ_2          = FASTQ_DIR + "{sample}_2.trimmed.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_UNPAIRED_FASTQ_1 = FASTQ_DIR + "{sample}_unpaired_1.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_UNPAIRED_FASTQ_2 = FASTQ_DIR + "{sample}_unpaired_2.%s" % FASTQ_SUFFIX

TRIMMED_FASTQ                   = FASTQ_DIR + "{sample}_{ext}.trimmed.%s" % FASTQ_SUFFIX


MAPPING_DIR = "ALIGNMENT/"
makedirs(ABSOLUTE_PATH + MAPPING_DIR)

SAI_FILE        = MAPPING_DIR + "{sample}_{ext}.sai" 
SAI_FILE_1      = MAPPING_DIR + "{sample}_1.sai"
SAI_FILE_2      = MAPPING_DIR + "{sample}_2.sai"
SINGLE_SAI_FILE = MAPPING_DIR + "{sample}_s.sai"

SAM_FILE       = MAPPING_DIR + "{sample}.sam"
BAM_FILE       = MAPPING_DIR + "{sample}.bam"
INDEXED_BAM    = MAPPING_DIR + "{sample}.bai"
MARKED_FILE    = MAPPING_DIR + "{sample}.noDup.bam"
REALIGNED_FILE = MAPPING_DIR + "{sample}.noDup.realigned.bam"

RESULTS_DIR = "RESULTS/"
makedirs(ABSOLUTE_PATH + RESULTS_DIR)
RECALIBRATED_BAM       = RESULTS_DIR + "{sample}.noDup.realigned.recalibrated.bam"
RNAEDITOR_FLAGSTATS    = RESULTS_DIR + "{sample}.flagstats"
RNAEDITOR_COVERAGE_BED = RESULTS_DIR + "{sample}.coverage.bed"
STRINGTIE_GENES        = RESULTS_DIR + "{sample}.gene_exp.tsv"
COMBINED_EDITING_SITES = RESULTS_DIR + "{sample}.all_editing_sites.vcf"

PURIFICATION_DIR = "PURIFICATION/"
makedirs(ABSOLUTE_PATH + PURIFICATION_DIR)
PCR_METRICS                = PURIFICATION_DIR + "{sample}.pcr_metrics"
INTERVAL_FILE              = PURIFICATION_DIR + "{sample}.indels.intervals"
RECAL_FILE                 = PURIFICATION_DIR + "{sample}.recalSpots.grp"
VCF_FILE                   = PURIFICATION_DIR + "{sample}.vcf"
SNP_METRICS                = PURIFICATION_DIR + "{sample}.snp.metrics"

FILTERED_STATS            = PURIFICATION_DIR + "{sample}.filtered.stats"
FILTERED_VCF_REPEAT       = PURIFICATION_DIR + "{sample}.repeat.filtered.vcf"
FILTERED_VCF_NON_REPEAT   = PURIFICATION_DIR + "{sample}.nonrepeat.filtered.blat.vcf"
FASTA_OF_READS_WITH_SNPS  = PURIFICATION_DIR + "{sample}.reads_with_snps.fasta"

BLAT_PSL_FILE              = PURIFICATION_DIR + "{sample}.psl"

samples = []
for line in open(config["samples"]):
    # Use the split("\t") command here so we can keep sample 
    # ids paired with annotation information. 
    samples.append(line.strip().split("\t")[0])

rule all: 
    input: 
        expand(RECALIBRATED_BAM, sample=samples),
        #expand(STRINGTIE_GTF_FINAL, sample=samples),
        #expand(RNAEDITOR_FLAGSTATS, sample=samples),
        #expand(RNAEDITOR_COVERAGE_BED, sample=samples),
        # The file containing editing sites. 
        #expand(COMBINED_EDITING_SITES, sample=samples),

# =========================================================
# Trim FASTQ files
# =========================================================

'''
# Possible module solution? 
# https://groups.google.com/forum/#!topic/snakemake/e0XNmXqL7Bg
import socket
if "cluster" in socket.gethostname():
    shell.prefix("module load Python; ")
rule:
    input: a
    output: b
    shell: "python script.py"

rule single_trim_fastqs:
    input: SINGLE_FASTQ
    output: SINGLE_TRIMMED_FASTQ
    params:
        trimmomatic_path = config['trimmomatic_path'],
        trimmomatic_args = config['trimmomatic_args'],
        java_module = "module load java-1.8.0_40"
    priority: 2
    threads: 8
    shell: """
    {params.java_module}
    java -jar {params.trimmomatic_path} SE -phred33 \
        {input} {output} {params.trimmomatic_args}
    """
'''

rule prefetch:
    output: SRA
    shell: """ prefetch {wildcards.sample} """


rule fastqdump:
    input: "sra/{sample}.sra" 
    output:
        fastq_1 = PAIRED_FASTQ_1,
        fastq_2 = PAIRED_FASTQ_2
    params: fastq_dir = "../FASTQS"
    shell: """
    cd sra
    fastq-dump --split-3 --O {params.fastq_dir} {wildcards.sample} ;
    touch -c {output.fastq_1};
    touch -c {output.fastq_2};
    """


rule trim_fastqs:
    input:
        fastq_1 = PAIRED_FASTQ_1,
        fastq_2 = PAIRED_FASTQ_2
    output:
        trimmed_pd_fq_1   = temp(PAIRED_TRIMMED_FASTQ_1),
        trimmed_pd_fq_2   = temp(PAIRED_TRIMMED_FASTQ_2),
        trimmed_upd_fq_1 = temp(PAIRED_TRIMMED_UNPAIRED_FASTQ_1),
        trimmed_upd_fq_2 = temp(PAIRED_TRIMMED_UNPAIRED_FASTQ_2),
    params:
        trimmomatic_path = config['trimmomatic_path'],
        trimmomatic_args = config['trimmomatic_args'],
        java_module = "module load java-1.8.0_40"
    priority: 2
    threads: 8
    shell: """
    {params.java_module}
    java -jar {params.trimmomatic_path} PE -phred33    \
    {input.fastq_1} {input.fastq_2}                    \
    {output.trimmed_pd_fq_1} {output.trimmed_upd_fq_1} \
    {output.trimmed_pd_fq_2} {output.trimmed_upd_fq_2} \
    {params.trimmomatic_args}
    """


# =========================================================
# INITAL ALIGNMENT
# =========================================================

rule align_fastq_to_genome:
    input: TRIMMED_FASTQ
    output: temp(SAI_FILE)
    params:
        max_diff = MAX_DIFF,
        seed_diff = SEED_DIFF,
        reference_genome = REFERENCE_GENOME
    threads: THREADS
    shell: """
    bwa aln -t {threads}          \
        -n {params.max_diff}      \
        -k {params.seed_diff}     \
        {params.reference_genome} \
        {input} > {output} 
    """

rule single_sai_to_bam:
    input:
        sai   = SINGLE_SAI_FILE,
        fastq = SINGLE_TRIMMED_FASTQ
    params:
        read_group_header = "@RG\\tID:bwa\\tSM:A\\tPL:ILLUMINA\\tPU:HiSEQ2000",
        reference_genome = REFERENCE_GENOME
    output: temp(BAM_FILE)
    threads: THREADS
    shadow: "shallow"
    shell: """
    bwa samse -r "{params.read_group_header}"               \
        {params.reference_genome} {input.sai} {input.fastq} \
        | samtools sort -@ {threads} -o {output} -
    """

rule paired_sai_to_bam:
    input:
        sai_1 = SAI_FILE_1,
        sai_2 = SAI_FILE_2,
        fastq_1 = PAIRED_TRIMMED_FASTQ_1,
        fastq_2 = PAIRED_TRIMMED_FASTQ_2
    params:
        read_group_header = "@RG\\tID:bwa\\tSM:A\\tPL:ILLUMINA\\tPU:HiSEQ2000",
        reference_genome = REFERENCE_GENOME
    priority: 5
    output: temp(BAM_FILE)
    threads: THREADS
    shadow: "shallow"
    shell: """
    bwa sampe  -r "{params.read_group_header}" \
       {params.reference_genome}               \
       {input.sai_1}   {input.sai_2}           \
       {input.fastq_1} {input.fastq_2}         \
       | samtools sort -@ {threads} -o {output} -
    """

rule index_bam:
    input:  BAM_FILE
    threads: THREADS
    output: temp(INDEXED_BAM)
    shell: """samtools index {input} {output}"""

rule mark_PCR_duplicates: 
    input: BAM_FILE 
    params: 
        picard_tools = PICARD_PATH,
        tmp_path = TMP_PATH,
        picard_args = " VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=true "
    threads: THREADS
    output: 
         marked_file = temp(MARKED_FILE), 
         pcr_metrics = temp(PCR_METRICS),
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -Djava.io.tmpdir={params.tmp_path} \
        -jar {params.picard_tools}                  \
        INPUT={input}                               \
        OUTPUT={output.marked_file}                 \
        METRICS_FILE={output.pcr_metrics} {params.picard_args} TMP_DIR={params.tmp_path}
    """

rule identify_regions_for_realignment:
    input: MARKED_FILE
    params:
        gatk             = GATK_PATH,
        reference_genome = REFERENCE_GENOME,
        tmp_path         = TMP_PATH
    threads: THREADS 
    output: INTERVAL_FILE 
    threads: THREADS
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -Djava.io.tmpdir={params.tmp_path} -jar {params.gatk} -nt {threads} -T RealignerTargetCreator -R {params.reference_genome} -I {input} -o {output} -l ERROR """   

rule perform_realignment:
    input: 
        marked_file = MARKED_FILE,
        interval_file = INTERVAL_FILE
    params: 
        gatk             = GATK_PATH, 
        reference_genome = REFERENCE_GENOME,
        tmp_path         = TMP_PATH
    threads: THREADS
    output: REALIGNED_FILE 
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -Djava.io.tmpdir={params.tmp_path} \
         -jar {params.gatk}                         \
         -T IndelRealigner                          \
         -R {params.reference_genome}               \
         -I {input.marked_file}                     \
         -l ERROR                                   \
         -targetIntervals {input.interval_file}     \
         -o {output}
    """

# java -Xmx16G -jar GenomeAnalysisTK.jar -T BaseRecalibrator -l ERROR -R /media/tyler/DATA/Storage/references/9606/e83/Homo_sapiens.GRCh38.dna.primary_assembly.fa -knownSites /media/tyler/DATA/Storage/references/9606/e83/dbSNP.vcf -I SRR787276.noDup.realigned.bam  -cov CycleCovariate -cov ContextCovariate -o SRR787276.recalSpots.grp
rule find_quality_score_recalibration_spots:
    input: 
        realigned_file = REALIGNED_FILE
    output: 
        recalibrated_file = RECAL_FILE
    threads: THREADS
    params: 
        gatk = GATK_PATH, 
        reference_genome = REFERENCE_GENOME,
        snps = SNPS
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -jar {params.gatk}    \
        -T BaseRecalibrator            \
        -l ERROR                       \
        -R {params.reference_genome}   \
        -knownSites {params.snps}      \
        -I {input.realigned_file}      \
        -cov CycleCovariate            \
        -cov ContextCovariate          \
        -o {output.recalibrated_file}                         
    """

#  java -Xmx16G -jar GenomeAnalysisTK.jar -T PrintReads -l ERROR -R /media/tyler/DATA/Storage/references/9606/e83/Homo_sapiens.GRCh38.dna.primary_assembly.fa -I SRR787276.noDup.realigned.bam -BQSR SRR787276.recalSpots.grp -o SRR787276.noDup.realigned.recalibrated.bam
rule quality_score_recalibration:
    input: 
        realigned_file = REALIGNED_FILE,
        recalibrated_file = RECAL_FILE,
    output: RECALIBRATED_BAM 
    threads: THREADS
    params: 
        gatk = GATK_PATH, 
        reference_genome = REFERENCE_GENOME
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -jar {params.gatk}     \
        -T PrintReads                   \
        -l ERROR                        \
        -R {params.reference_genome}    \
        -I {input.realigned_file}       \
        -BQSR {input.recalibrated_file} \
        -o {output}
    """

# java -Xmx16G -jar GenomeAnalysisTK.jar -T UnifiedGenotyper -R /media/tyler/DATA/Storage/references/9606/e83/Homo_sapiens.GRCh38.dna.primary_assembly.fa -glm SNP  -I SRR787276.noDup.realigned.recalibrated.bam -D /media/tyler/DATA/Storage/references/9606/e83/dbSNP.vcf -o SRR787276.vcf -metrics SRR787276.snp_metrics -nt 8 -l ERROR -stand_call_conf 0 -stand_emit_conf 0 -A Coverage -A AlleleBalance -A BaseCounts 
# CallEditingSites
# -stand_emit_conf {params.stand_emit}
rule unified_genotyper:
    input: RECALIBRATED_BAM
    threads: THREADS
    params:
        gatk = GATK_PATH, 
        reference_genome = REFERENCE_GENOME,
        snps = SNPS,
        threads = THREADS,
        stand_call = STAND_CALL,
        stand_emit = STAND_EMIT,
    priority: 500
    output: 
        snp_metrics = SNP_METRICS, 
        vcf         = VCF_FILE
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -jar {params.gatk}                                           \
         -T UnifiedGenotyper -R {params.reference_genome} -glm SNP -I {input} \
         -D {params.snps} -o {output.vcf} -metrics {output.snp_metrics}       \
         -nt {params.threads} -l ERROR -stand_call_conf {params.stand_call}   \
         -A Coverage -A AlleleBalance -A BaseCounts
    """

# =========================================================
# Filter out potential fasle positives
# =========================================================

rule filter_false_positives: 
    input: 
        unfiltered_sites = VCF_FILE,
        bam = RECALIBRATED_BAM
    params: 
        reference_genome = REFERENCE_GENOME,
        splice_junctions = SPLICE_REGIONS
        repetitive_elements = REPEAT_REGIONS
    output:
        filtering_stats = FILTERED_STATS,
        filtered_sites_in_repeat_region = FILTERED_VCF_REPEAT,
        filtered_sites_in_nonrepeat_region = FILTERED_VCF_NON_REPEAT,
        filtering_fasta = FASTA_OF_READS_WITH_SNPS
    shell: """
    src/programs/esfilter/esfilter.py                 \
        --vcf {input.unfiltered_sites}                \
        --bam {input.bam}                             \
        \
        --reference-genome {params.reference_genome}     \
        --splice-junctions {params.splice_junctions}     \
        --repeat-regions   {params.repetitive_elements}  \
        \
        --filter-stats                {output.filtering_stats}                     \
        --repeat-regions-out-vcf      {output.filtered_sites_in_repeat_region}     \
        --non-repeat-regions-out-vcf  {output.filtered_sites_in_nonrepeat_region}  \
        --in-repeat-regions-out-fasta {output.filtering_fasta}
    """

rule run_blat:
    input: FASTA_OF_READS_WITH_SNPS
    threads: 8
    params: ref_genome = REFERENCE_GENOME,
    output: BLAT_PSL_FILE
    shell: """
    blat               \
        -stepSize=5    \
        -repMatch=2253 \
        -minScore=20   \
        -minIdentity=0 \
        -noHead {params.ref_genome} \
        {input} {output} 
    """

rule filter_psl: 
    input: 
        vcf = FILTERED_VCF_NON_REPEAT, 
        psl = BLAT_PSL_FILE
    threads: 8
    output: BLAT_FILTERED_VCF_NON_REPEAT
    shell: """python3 src/filter_psl_file.py --vcf {input.vcf} --psl {input.psl} > {output}"""

# =====
# Get stats and gene expression info from 
# =========
# It is possible to generate a GTF file here but since the aligner is 
# not using both paired reads I am not sure if this is a good idea. 
# -o {output.gtf}
rule find_gene_expression:
    input: RECALIBRATED_BAM
    params: ref_gtf = config["gtf"]
    threads: THREADS
    output: GENE_EXPRESSION
    shell: """stringtie -e -p 8 -G {params.ref_gtf} {input} -A {output} """

rule flagstats:
    input: RECALIBRATED_BAM  # RNAEDITOR_BAM
    output: RNAEDITOR_FLAGSTATS
    shell: """ samtools flagstat {input} > {output} """

rule find_coverage:
    input: RECALIBRATED_BAM  # RNAEDITOR_BAM
    params: genome = GENOME_LENGTHS
    output: RNAEDITOR_COVERAGE_BED
    shell: """ samtools view -b {input} | genomeCoverageBed -ibam stdin -g {params.genome} > {output} """
 
# Recombine ALU and non-ALU and we're done!
rule combine_filtered_editing_sites: 
    input: 
        repeats    = FILTERED_VCF_REPEAT,
        non_repeat = BLAT_FILTERED_VCF_NON_REPEAT
    threads: 8
    output: COMBINED_EDITING_SITES 
    shell: """cat {input.repeats} {input.non_repeat} > {output} """        










