"""
{
    "samples":      "samples.txt",
    "merged_sites": "islands_me3_mc5_ml20_mp5_ep100.bed",
    "islands":      "merged_sites_me3_mc5_ms3.vcf"
}
"""
import os

# ============================================================================
#                              Helper functions
# ============================================================================

def check_file(file_path, file_not_found_message='File "%s" not found.'):
    assert os.path.isfile(file_path), file_not_found_message % file_path
    return file_path

def get_sample_names(sample_file):
    samples = []
    for line in open(sample_file):
        # Use the split("\t") command here so we can keep sample
        # ids paired with annotation information.
        samples.append(line.strip().split("\t")[0])
    return samples


#MIN_EDITING  = int(config["min_editing"])
#MIN_COVERAGE = int(config["min_coverage"])

QSCORE       = config["qscores"] if type(config["qscores"]) is list else [config["qscores"]]

MERGED_SITES = check_file(config["merged_sites"])
ISLANDS      = check_file(config["islands"])

SAMPLES      = get_sample_names(config["samples"])

# =================================================================================================
#                              Initial Files
# =================================================================================================
BAM_DIR = "RESULTS/"
BAM_FILE = BAM_DIR + "{sample}.noDup.realigned.recalibrated.bam"
BAI_FILE = BAM_DIR + "{sample}.noDup.realigned.recalibrated.bai"
#BAM_FILE = SAMPLES
# =================================================================================================
#                              Files to Create
# =================================================================================================

EPK_DIR  = "EPK/"
SAMPLE_EPK  = EPK_DIR + "{sample}.q{qscore}.sample_EPK.tsv"
ISLAND_EPK  = EPK_DIR + "{sample}.q{qscore}.island_EPK.tsv"
SITE_EPK    = EPK_DIR + "{sample}.q{qscore}.edsite_EPK.tsv"

# =================================================================================================
#                                   ALL
# =================================================================================================

rule all:
    input:
        expand(SAMPLE_EPK, sample=SAMPLES, qscore=QSCORE),
        # expand(ISLAND_EPK, sample=SAMPLES, qscore=QSCORE),
        # expand(SITE_EPK,   sample=SAMPLES, qscore=QSCORE)

# =================================================================================================
#                                  Rules
# =================================================================================================

rule find_sample_epk:
    input: BAM_FILE
    params:
        merged_editing_sites = MERGED_SITES
    output: SAMPLE_EPK
    shell: """
    dretools sample-epk                             \
        --name       {wildcards.sample}             \
        --min-qscore {wildcards.qscore}             \
        --vcf        {params.merged_editing_sites}  \
        --alignment  {input} > {output}
    """

rule find_island_epk:
    input: BAM_FILE
    params:
        merged_editing_sites = MERGED_SITES,
        regions = ISLANDS
    output: ISLAND_EPK
    shell: """
    dretools region-epk                             \
        --vcf        {params.merged_editing_sites}  \
        --regions    {params.regions}               \
        --min-qscore {wildcards.qscore}             \
        --alignment {input} > {output}
    """

rule find_site_epk:
    input: BAM_FILE
    params: merged_editing_sites = MERGED_SITES
    output: SITE_EPK
    shell: """
    dretools edsite-epk                      \
        --vcf {params.merged_editing_sites}  \
        --min-qscore {wildcards.qscore}      \
        --alignment  {input} > {output}
    """





