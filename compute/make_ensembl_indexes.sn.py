"""
A pipeline for generating indexes and reference files. 
==============================================================================
"""
from snakemake.utils import makedirs
from os.path import abspath
ABSOLUTE_PATH = os.path.abspath("") + "/"

REFERENCE = "references/"
REFERENCE_GENOME_LINK = "ftp://ftp.ensembl.org/pub/release-94/fasta/homo_sapiens/dna/"
REFERENCE_GENOME = REFERENCE + "{tax_id}/{version}/{genome}.faa"

# @TODO: Figure out how to make restrictions as to which rules can run.
# Only one download should be active at a time. 

# @TODO: Add try except to handle top_level issues.
rule download_genome: 
    params: download_link = REFERENCE_GENOME_LINK
    output: REFERENCE_GENOME
    shell: """ wget {params.download_link} --output-file {output} """ 

rule download_gene_annotations: 
    params: download_link = REFERENCE_GENOME_LINK
    output: REFERENCE_GENOME
    shell: """ wget {params.download_link} --output-file {output} """ 

rule download_snps: 
    params: download_link = REFERENCE_GENOME_LINK
    output: REFERENCE_GENOME
    shell: """ wget {params.download_link} --output-file {output} """ 

# Unstranded for now as SNPs
# Ensembl GTFs are one-based. Therefore, need to convert to zero based when converting to bed. 
# https://arnaudceol.wordpress.com/2014/09/18/chromosome-coordinate-systems-0-based-1-based/
rule make_exon_bed: 
     input: config["gtf"]
     threads: THREADS
     priority: 500
     output: EXON_BED 
     shell: 
         """ grep -P "\texon\t" {input} | """ 
         """ awk '{{print $1"\\t"$4-1"\\t"$5}}' | """
         """ grep -v "\\." """
         """ | sort | uniq > {output} """
     
 
# This rule builds a bed file containing a given distance on either side of the splice junctions.
rule make_splice_junction_bed: 
     input:  EXON_BED
     threads: 8
     params: distance = DISTANCE,
     priority: 50
     output: SPLICE_JUNCTIONS 
     shell: 
         """ awk '{{print $1"\t"$2-{params.distance}"\t"$2+{params.distance}}}' {input} >  {output} """
         """ awk '{{print $1"\t"$3-{params.distance}"\t"$3+{params.distance}}}' {input} >> {output} """


rule make_splice_junctions: 
    input: "genes.gtf"
    output: "splice_sites.bed"
    shell: """
        head {input} > {output} 
    """
rule make_exons: 
    input: "genes.gtf"
    output: "exons.bed"
    shell: """
        head {input} > {output} 
    """

# Often used for after analysis annotation
rule make_gene_id_to_gene_name_and_biotype_map:
    input: "genes.gtf"
    output: "gene_ac_to_name_and_biotype.tsv"
    shell: """
        head {input} > {output} 
    """
rule make_transcript_id_to_transcript_name_and_biotype_map:
    input: "genes.gtf"
    output: "gene_ac_to_name_and_biotype.tsv"
    shell: """
        head {input} > {output} 
    """

# @TOOD: Snakemake is going to hate this, how to account for an unknown number of output files?
rule make_hisat2_index: 
    input: 
        reference_genome = "genome.fa",
        splice_sites = "splice_sites.bed",
        exons = "exons.bed",
    output: "genome.tran"
    threads: 32
    """
    hisat2-build --threads {threads} 
        {input.reference_genome}
        --ss   {input.splice_sites} 
        --exon {input.exons} 
        {output}
    """

# @TOOD: Snakemake is going to hate this, how to account for an unknown number of output files?
rule make_bowtie_index: 
    input: 
        reference_genome = "genome.fa",
        splice_sites = "splice_sites.bed",
        exons = "splice_sites.bed",
    output: "genome.bt2"
    threads: 12
    """
    bowtie2-build --threads {threads} \
        {input.reference_genome}      \
        {input.reference_genome}
    touch -c {output}
    """

# bowtie
rule make_bwa_index: 
    input: 
        reference_genome = "genome.fa",
        splice_sites = "splice_sites.bed",
        exons = "splice_sites.bed",
    output: "genome.bwa"
    threads: 12
    """
    touch -c {input.reference_genome}
    touch -c {output}
    """

# STAR
rule make_star_index: 
    input: 
        reference_genome = "genome.fa",
        splice_sites = "splice_sites.bed",
        exons = "splice_sites.bed",
    output: "genome.bwa"
    threads: 12
    """
    touch -c {input.reference_genome}
    touch -c {output}
    """


 wget -qO- ftp://ftp.ensembl.org/pub/release-83/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz |gunzip -c > Homo_sapiens.GRCh38.dna.primary_assembly.fa bwa index Homo_sapiens.GRCh38.dna.primary_assembly.fa; samtools faidx Homo_sapiens.GRCh38.dna.primary_assembly.fa
Create Dictionary of Reference genome:
java -jar /usr/local/bin/picard-tools/CreateSequenceDictionary.jar REFERENCE=Homo_sapiens.GRCh38.dna.primary_assembly.fa OUTPUT=Homo_sapiens.GRCh38.dna.primary_assembly.dict
Gene Annotation:
wget -qO- ftp://ftp.ensembl.org/pub/release-83/gtf/homo_sapiens/Homo_sapiens.GRCh38.83.gtf.gz |gunzip -c > Homo_sapiens.GRCh38.83.gtf
dbSNP Database:
wget -qO- ftp://ftp.ensembl.org/pub/release-83/variation/vcf/homo_sapiens/Homo_sapiens.vcf.gz |gunzip -c |awk 'BEGIN{FS="\t";OFS="\t"};match($5,/\./){gsub(/\./,"N",$5)};$5 == "" && $1 !~ /^#/ {gsub("","N",$5)};$3 ~ /rs193922900/ {$5="TN"};$3 ~ /rs59736472/ {$5="AN"};$5 ~ /H/ {gsub(/H/,"N",$5)};{print $0}' dbSNP.vcf
ESP Database:
wget -qO- ftp://ftp.ensembl.org/pub/release-83/variation/vcf/homo_sapiens/ESP65*.vcf.gz |gunzip -c |grep -v ^## |grep -v rs[0-9][0-9] > ESP.vcf
HAPMAP Database:
wget -qO- ftp://ftp.ensembl.org/pub/release-83/variation/vcf/homo_sapiens/*HAPMAP*.vcf.gz |gunzip -c |grep -v ^## |grep -v rs[0-9][0-9] > HAPMAP.vcf
Download Alu Regions:

    #download Alu regions from the repeat masker
        Link: http://genome.ucsc.edu/cgi-bin/hgTables
        group: Variation and Repeats
        track: RepeatMasker
        table: rmsk
        output format: BED

#run this awk command to make the alu region compatible to the ensemble annotation
awk 'BEGIN{FS="\t";OFS="\t"} match($1,/chr/){$1 = substr($1,4)}{print $0}' yourFile.bed > youFile_noCHR.bed 

#!/bin/sh

#
# Downloads sequence for the GRCh38 release 84 version of H. sapiens (human) from
# Ensembl.
#
# Note that Ensembl's GRCh38 build has three categories of compressed fasta
# files:
#
# The base files, named ??.fa.gz
#
# By default, this script builds and index for just the base files,
# since alignments to those sequences are the most useful.  To change
# which categories are built by this script, edit the CHRS_TO_INDEX
# variable below.
#

ENSEMBL_RELEASE=84
ENSEMBL_GRCh38_BASE=ftp://ftp.ensembl.org/pub/release-${ENSEMBL_RELEASE}/fasta/homo_sapiens/dna
ENSEMBL_GRCh38_GTF_BASE=ftp://ftp.ensembl.org/pub/release-${ENSEMBL_RELEASE}/gtf/homo_sapiens
GTF_FILE=Homo_sapiens.GRCh38.${ENSEMBL_RELEASE}.gtf

get() {
	file=$1
	if ! wget --version >/dev/null 2>/dev/null ; then
		if ! curl --version >/dev/null 2>/dev/null ; then
			echo "Please install wget or curl somewhere in your PATH"
			exit 1
		fi
		curl -o `basename $1` $1
		return $?
	else
		wget $1
		return $?
	fi
}

HISAT2_BUILD_EXE=./hisat2-build
if [ ! -x "$HISAT2_BUILD_EXE" ] ; then
	if ! which hisat2-build ; then
		echo "Could not find hisat2-build in current directory or in PATH"
		exit 1
	else
		HISAT2_BUILD_EXE=`which hisat2-build`
	fi
fi

HISAT2_SS_SCRIPT=./hisat2_extract_splice_sites.py
if [ ! -x "$HISAT2_SS_SCRIPT" ] ; then
	if ! which hisat2_extract_splice_sites.py ; then
		echo "Could not find hisat2_extract_splice_sites.py in current directory or in PATH"
		exit 1
	else
		HISAT2_SS_SCRIPT=`which hisat2_extract_splice_sites.py`
	fi
fi

HISAT2_EXON_SCRIPT=./hisat2_extract_exons.py
if [ ! -x "$HISAT2_EXON_SCRIPT" ] ; then
	if ! which hisat2_extract_exons.py ; then
		echo "Could not find hisat2_extract_exons.py in current directory or in PATH"
		exit 1
	else
		HISAT2_EXON_SCRIPT=`which hisat2_extract_exons.py`
	fi
fi

rm -f genome.fa
F=Homo_sapiens.GRCh38.dna.primary_assembly.fa
if [ ! -f $F ] ; then
	get ${ENSEMBL_GRCh38_BASE}/$F.gz || (echo "Error getting $F" && exit 1)
	gunzip $F.gz || (echo "Error unzipping $F" && exit 1)
	mv $F genome.fa
fi

if [ ! -f $GTF_FILE ] ; then
       get ${ENSEMBL_GRCh38_GTF_BASE}/${GTF_FILE}.gz || (echo "Error getting ${GTF_FILE}" && exit 1)
       gunzip ${GTF_FILE}.gz || (echo "Error unzipping ${GTF_FILE}" && exit 1)
       ${HISAT2_SS_SCRIPT} ${GTF_FILE} > genome.ss
       ${HISAT2_EXON_SCRIPT} ${GTF_FILE} > genome.exon
fi

CMD="${HISAT2_BUILD_EXE} -p 4 genome.fa --ss genome.ss --exon genome.exon genome_tran"
echo Running $CMD
if $CMD ; then
	echo "genome index built; you may remove fasta files"
else
	echo "Index building failed; see error message"
fi
