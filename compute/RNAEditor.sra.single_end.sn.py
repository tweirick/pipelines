import snakemake
from snakemake.utils import makedirs
from os.path import abspath


#localrules: prefetch
#, combine_filtered_editing_sites

ABSOLUTE_PATH    = abspath("") + "/"
REPEATS          = config["repeats"]
DISTANCE         = config["distance"]
SNPS             = config["SNPs"]
STAND_CALL       = config["stand_call"]
STAND_EMIT       = config["stand_emit"]
THREADS          = config["threads"]
GATK_PATH        = config["gatk_path"]
PICARD_PATH      = config["picard_path"]

TMP_PATH = ABSOLUTE_PATH + "EDITING_TMP"
makedirs(TMP_PATH)

try:
    THREADS = int(THREADS)
except ValueError:
    exit("Error: Threads must be an integer.")

MAX_DIFF         = config["max_diff"]
SEED_DIFF        = config["seed_diff"]

REFERENCE_GENOME = config["reference_genome"]
SPLICE_REGIONS = config["splice"]
REPEAT_REGIONS = config["repeats"]
# ============================================================================
#                             Rule IO
# ============================================================================
#SRA_FOLDER = "/scratch/global/SULab/ncbi/public/sra/"
SAMPLE = "{sample}"

if float(snakemake.__version__[0]) > 3:
    SAMPLE_CON = "{sample, \w+}"
else:
   SAMPLE_CON = SAMPLE

SRA_FOLDER = "sra/"
SRA = SRA_FOLDER + "%s.sra"

FASTQ_DIR = "FASTQS/"
SINGLE_FASTQ = FASTQ_DIR + "%s.fastq"        
TFQ = "TRIMMED_FASTQS/"
SINGLE_TRIMMED_FASTQ  = TFQ + "%s.trimmed.fastq" 

MAPPING_DIR = "ALIGNMENT/"
makedirs(ABSOLUTE_PATH + MAPPING_DIR)
SINGLE_SAI_FILE = MAPPING_DIR + "%s.sai"
BAM_FILE        = MAPPING_DIR + "%s.bam"
INDEXED_BAM     = MAPPING_DIR + "%s.bai"
MARKED_FILE     = MAPPING_DIR + "%s.noDup.bam"
PCR_METRICS     = MAPPING_DIR + "%s.pcr_metrics"

REALIGNED_FILE  = MAPPING_DIR + "%s.noDup.realigned.bam"

RESULTS_DIR = "RESULTS/"
makedirs(ABSOLUTE_PATH + RESULTS_DIR)
RECALIBRATED_BAM       = RESULTS_DIR + "%s.noDup.realigned.recalibrated.bam"
RNAEDITOR_FLAGSTATS    = RESULTS_DIR + "%s.flagstats"
RNAEDITOR_COVERAGE_BED = RESULTS_DIR + "%s.coverage.bed"
GENE_EXPRESSION        = RESULTS_DIR + "%s.gene_exp.tsv"
COMBINED_EDITING_SITES = RESULTS_DIR + "%s.all_editing_sites.vcf"

PURIFICATION_DIR = "PURIFICATION/"
makedirs(ABSOLUTE_PATH + PURIFICATION_DIR)

INTERVAL_FILE              = PURIFICATION_DIR + "%s.indels.intervals"
RECAL_FILE                 = PURIFICATION_DIR + "%s.recalSpots.grp"
VCF_FILE                   = PURIFICATION_DIR + "%s.init.vcf"
SNP_METRICS                = PURIFICATION_DIR + "%s.snp.metrics"

FILTERED_STATS               = PURIFICATION_DIR + "%s.filtered.stats"
FILTERED_VCF_REPEAT          = PURIFICATION_DIR + "%s.repeats.vcf"
FILTERED_VCF_NON_REPEAT      = PURIFICATION_DIR + "%s.nonrepeats.vcf"
FASTA_OF_READS_WITH_SNPS     = PURIFICATION_DIR + "%s.nonreeat_sites.fasta"
BLAT_PSL_FILE                = PURIFICATION_DIR + "%s.psl"
BLAT_FILTERED_VCF_NON_REPEAT = PURIFICATION_DIR + "%s.passing_nonrepeats.vcf"

samples = []
for line in open(config["samples"]):
    # Use the split("\t") command here so we can keep sample 
    # ids paired with annotation information. 
    if line[0] != "#":
        samples.append(line.strip().split("\t")[0])

ruleorder: unified_genotyper > filter_false_positives > filter_psl
rule all: 
    input:
        #expand(RECALIBRATED_BAM, sample=samples),
        expand(GENE_EXPRESSION        % SAMPLE, sample=samples),
        expand(RNAEDITOR_FLAGSTATS    % SAMPLE, sample=samples),
        expand(COMBINED_EDITING_SITES % SAMPLE, sample=samples),

# =========================================================
# Trim FASTQ files
# =========================================================

#rule prefetch:
#    output: SRA
#    shell: """ prefetch {wildcards.sample} """

rule fastqdump:
    input: SRA % SAMPLE
    output: temp(SINGLE_FASTQ % SAMPLE_CON)
    shell: """fastq-dump --stdout {input} > {output} """   

rule single_trim_fastqs:
    input: SINGLE_FASTQ % SAMPLE
    output: SINGLE_TRIMMED_FASTQ % SAMPLE_CON
    params:
        trimmomatic_path = config['trimmomatic_path'],
        trimmomatic_args = config['trimmomatic_args'],
        java_module = "module load java-1.8.0_40"
    priority: 2
    threads: 8
    shell: """
    {params.java_module}
    java -jar {params.trimmomatic_path} SE -phred33 \
        {input} {output} {params.trimmomatic_args}
    """

# =========================================================
# INITAL ALIGNMENT
# =========================================================

rule align_fastq_to_genome:
    input: SINGLE_TRIMMED_FASTQ % SAMPLE
    output: temp(SINGLE_SAI_FILE % SAMPLE_CON)
    params:
        max_diff = MAX_DIFF,
        seed_diff = SEED_DIFF,
        reference_genome = REFERENCE_GENOME
    threads: THREADS
    shell: """
    bwa aln -t {threads}          \
        -n {params.max_diff}      \
        -k {params.seed_diff}     \
        {params.reference_genome} \
        {input} > {output} 
    """

rule single_sai_to_bam:
    input:
        sai   = SINGLE_SAI_FILE % SAMPLE,
        fastq = SINGLE_TRIMMED_FASTQ % SAMPLE
    params:
        read_group_header = "@RG\\tID:bwa\\tSM:A\\tPL:ILLUMINA\\tPU:HiSEQ2000",
        reference_genome = REFERENCE_GENOME
    output: temp(BAM_FILE % SAMPLE_CON)
    threads: THREADS
    shadow: "shallow"
    shell: """
    bwa samse -r "{params.read_group_header}"               \
        {params.reference_genome} {input.sai} {input.fastq} \
        | samtools sort -@ {threads} -o {output} -
    """

rule index_bam:
    input:  BAM_FILE % SAMPLE
    threads: THREADS
    output: INDEXED_BAM % SAMPLE_CON
    shell: """samtools index {input} {output}"""

rule mark_PCR_duplicates: 
    input: 
         bam = BAM_FILE % SAMPLE,
         bai = INDEXED_BAM % SAMPLE
    params: 
        picard_tools = PICARD_PATH,
        tmp_path     = TMP_PATH,
        picard_args  = " VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=true "
    threads: THREADS
    output: 
         marked_file = temp(MARKED_FILE % SAMPLE_CON), 
         pcr_metrics = temp(PCR_METRICS % SAMPLE_CON),
    shell: """
    touch {input.bai}
    module load java-1.8.0_40
    java -Xmx32G -Djava.io.tmpdir={params.tmp_path}  \
        -jar {params.picard_tools}                   \
        INPUT={input.bam}                            \
        OUTPUT={output.marked_file}                  \
        METRICS_FILE={output.pcr_metrics} {params.picard_args} TMP_DIR={params.tmp_path}
    """

rule identify_regions_for_realignment:
    input: MARKED_FILE % SAMPLE
    params:
        gatk             = GATK_PATH,
        reference_genome = REFERENCE_GENOME,
        tmp_path         = TMP_PATH
    threads: THREADS 
    output: INTERVAL_FILE % SAMPLE_CON
    threads: THREADS
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -Djava.io.tmpdir={params.tmp_path} -jar {params.gatk} -nt {threads} -T RealignerTargetCreator -R {params.reference_genome} -I {input} -o {output} -l ERROR """   

rule perform_realignment:
    input: 
        marked_file = MARKED_FILE % SAMPLE,
        interval_file = INTERVAL_FILE % SAMPLE
    params: 
        gatk             = GATK_PATH, 
        reference_genome = REFERENCE_GENOME,
        tmp_path         = TMP_PATH
    threads: THREADS
    output: REALIGNED_FILE % SAMPLE_CON
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -Djava.io.tmpdir={params.tmp_path} \
         -jar {params.gatk}                         \
         -T IndelRealigner                          \
         -R {params.reference_genome}               \
         -I {input.marked_file}                     \
         -l ERROR                                   \
         -targetIntervals {input.interval_file}     \
         -o {output}
    """

rule find_quality_score_recalibration_spots:
    input: 
        realigned_file = REALIGNED_FILE % SAMPLE
    output: 
        recalibrated_file = RECAL_FILE % SAMPLE
    threads: THREADS
    params: 
        gatk = GATK_PATH, 
        reference_genome = REFERENCE_GENOME,
        snps = SNPS
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -jar {params.gatk}    \
        -T BaseRecalibrator            \
        -l ERROR                       \
        -R {params.reference_genome}   \
        -knownSites {params.snps}      \
        -I {input.realigned_file}      \
        -cov CycleCovariate            \
        -cov ContextCovariate          \
        -o {output.recalibrated_file}                         
    """

rule quality_score_recalibration:
    input: 
        realigned_file = REALIGNED_FILE % SAMPLE,
        recalibrated_file = RECAL_FILE % SAMPLE,
    output: RECALIBRATED_BAM % SAMPLE_CON
    threads: THREADS
    params: 
        gatk = GATK_PATH, 
        reference_genome = REFERENCE_GENOME
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -jar {params.gatk}     \
        -T PrintReads                   \
        -l ERROR                        \
        -R {params.reference_genome}    \
        -I {input.realigned_file}       \
        -BQSR {input.recalibrated_file} \
        -o {output}
    """

rule unified_genotyper:
    input: RECALIBRATED_BAM % SAMPLE
    threads: THREADS
    params:
        gatk = GATK_PATH, 
        reference_genome = REFERENCE_GENOME,
        snps = SNPS,
        threads = THREADS,
        stand_call = STAND_CALL,
        stand_emit = STAND_EMIT,
    priority: 500
    output: 
        snp_metrics = SNP_METRICS % SAMPLE_CON, 
        vcf         = VCF_FILE % SAMPLE_CON
    shell: """
    module load java-1.8.0_40
    java -Xmx32G -jar {params.gatk}                                           \
         -T UnifiedGenotyper -R {params.reference_genome} -glm SNP -I {input} \
         -D {params.snps} -o {output.vcf} -metrics {output.snp_metrics}       \
         -nt {params.threads} -l ERROR -stand_call_conf {params.stand_call}   \
         -A Coverage -A AlleleBalance -A BaseCounts
    """

# =========================================================
# Filter out potential fasle positives
# =========================================================

#--min-edge-distance
#--homopolymer-distance
#--min-base-quality
#--min-mismatch-MIN_MISMATCH

rule filter_false_positives:
    input: 
        unfiltered_sites = VCF_FILE % SAMPLE,
        bam = RECALIBRATED_BAM % SAMPLE
    params: 
        reference_genome = REFERENCE_GENOME,
        splice_junctions = SPLICE_REGIONS,
        repetitive_elements = REPEAT_REGIONS
    output:
        filtered_sites_in_repeat_region = FILTERED_VCF_REPEAT % SAMPLE_CON,
        filtered_sites_in_nonrepeat_region = FILTERED_VCF_NON_REPEAT % SAMPLE_CON,
        filtering_fasta = FASTA_OF_READS_WITH_SNPS % SAMPLE_CON
    shell: """
    src/programs/esfilter/esfilter.py                 \
        --vcf {input.unfiltered_sites}                \
        --bam {input.bam}                             \
        \
        --reference-genome {params.reference_genome}     \
        --splice-junctions {params.splice_junctions}     \
        --repeat-regions   {params.repetitive_elements}  \
        \
        --repeat-regions-out-vcf      {output.filtered_sites_in_repeat_region}     \
        --non-repeat-regions-out-vcf  {output.filtered_sites_in_nonrepeat_region}  \
        --in-repeat-regions-out-fasta {output.filtering_fasta}
    """

rule run_blat:
    input: FASTA_OF_READS_WITH_SNPS % SAMPLE
    threads: 8
    params: ref_genome = REFERENCE_GENOME,
    output: BLAT_PSL_FILE % SAMPLE_CON
    shell: """
    blat               \
        -stepSize=5    \
        -repMatch=2253 \
        -minScore=20   \
        -minIdentity=0 \
        -noHead {params.ref_genome} \
        {input} {output} 
    """

rule filter_psl: 
    input: 
        vcf = FILTERED_VCF_NON_REPEAT % SAMPLE, 
        psl = BLAT_PSL_FILE % SAMPLE
    threads: 8
    output: BLAT_FILTERED_VCF_NON_REPEAT % SAMPLE
    shell: """python3 src/programs/filter_psl_file.py --vcf {input.vcf} --psl {input.psl} > {output}"""

# =====
# Get stats and gene expression info from 
# =========
# It is possible to generate a GTF file here but since the aligner is 
# not using both paired reads I am not sure if this is a good idea. 
# -o {output.gtf}
rule find_gene_expression:
    input: RECALIBRATED_BAM % SAMPLE
    params: ref_gtf = config["gtf"]
    threads: THREADS
    output: GENE_EXPRESSION % SAMPLE_CON
    shell: """stringtie -e -p 8 -G {params.ref_gtf} {input} -A {output} """

rule flagstats:
    input: RECALIBRATED_BAM  % SAMPLE
    output: RNAEDITOR_FLAGSTATS % SAMPLE_CON
    shell: """ samtools flagstat {input} > {output} """

# Recombine ALU and non-ALU and we're done!
rule combine_filtered_editing_sites: 
    input: 
        repeats    = FILTERED_VCF_REPEAT % SAMPLE,
        non_repeat = BLAT_FILTERED_VCF_NON_REPEAT % SAMPLE
    threads: 8
    output: COMBINED_EDITING_SITES % SAMPLE_CON
    shell: """cat {input.repeats} {input.non_repeat} > {output} """        

